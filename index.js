const express = require('express');
const app = express();
const {PORT = 8000} = process.env;
const path = require('path');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));




app.use('/assets', express.static(path.join( __dirname, 'public', 'static')))


app.get('/', (req, res) => {
    return res.sendFile( path.join( __dirname, 'public', 'index.html'));

})
app.get('/home', (req, res) => {
    return res.sendFile( path.join( __dirname, 'public', 'index.html'));

})
app.get('/about', (req, res) => {
    return res.sendFile( path.join( __dirname, 'public', 'about.html'));
    
})
app.get('/contacts', (req, res) => {
    return res.sendFile( path.join( __dirname, 'public', 'contact.html'));
    
})

app.post('/contacts', (req, res) => {
    res.status(200).send(`Data received successfull!<br> 
    Name: ${req.body.name}<br> 
    Email: ${req.body.email}<br> 
    Phone: ${req.body.phone}<br> 
    Text: ${req.body.textarea}`);
})


app.get('/menu', (req, res) => {
    return res.sendFile( path.join( __dirname, 'public', 'menu.html'));
    
})




app.listen(PORT, () => console.log(`Server running on ${PORT}`)); 