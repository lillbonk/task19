# Define our Base Image
FROM node:latest

#Create a working dir
WORKDIR /usr/src/app

#Copy dependencies
COPY package*.json ./
RUN npm install

#bundle src code
COPY . .

# Expose the port to be available outside
EXPOSE 8000

#run app
CMD ["node","index.js"]